def get(filename,foldername):
    # return input data as a dictionary
    import Imaging
    import numpy as np
    import scipy.io
       
    # spatial coordinates
    xmin = 0.0
    xmax = 1
    nx = 200
    dx = (xmax-xmin)/nx
    x = np.linspace(xmin,xmax,num = nx+1)
    hor = {'x': x, 'nx': nx, 'dx': dx, 'xmin': xmin, 'xmax' : xmax}
    
    zmin = 0.0
    zmax = 1
    nz = 200
    dz = (zmax-zmin)/nz
    z = np.linspace(zmin,zmax,num = nz+1)
    ver = {'z': z, 'nz': nz, 'dz': dz, 'zmin': zmin, 'zmax' : zmax}
    
    # time and frequency
    
    tmin = 0.0
    tmax = 10.0
    nt = 1000
    dt = (tmax-tmin)/nt
    t = np.linspace(tmin,tmax-dt,num = nt)
    time = {'t': t, 'nt': nt, 'dt': dt, 'tmin': tmin, 'tmax' : tmax}    
    
    wmin = -np.pi/dt
    wmax = np.pi/dt
    nw = nt
    dw = 2*np.pi/tmax
    w = np.linspace(wmin,wmax-dw,num = nw)
    ind = np.arange(550,560,1)
    
    freq = {'w': w, 'nw': nw, 'dw': dw, 'wmin': wmin, 'wmax' : wmax, 'ind': ind}    
    
    mesh = {'hor': hor, 'ver': ver, 'time': time, 'freq': freq}
    
    # boundary conditions
    boundary = {'left': 'abc', 'right': 'abc', 'top': 'abc', 'bottom': 'abc'}
    
    # ABC properties
    # a is frequency dependent part and b is frequency independent
    num_abc = 5
    abc_data = {'nabc': num_abc, 'a': np.ones((num_abc,1), dtype = np.float64) * (1.0-1.0j), 'b': np.zeros((num_abc,1), dtype = np.float64)}
    
    abc = {'left': abc_data, 'right': abc_data, 'top': abc_data, 'bottom': abc_data}
    
    # source
    # type: ricker
    #node_id_x = np.arange(7,nx+1, 8)
    node_id_x = np.array([nx/2+1])
    src_loc = node_id_x
#    node_id_x = np.array([8*nx/10]) 
    node_id = np.zeros( (node_id_x.size,2), dtype = np.int32)
    node_id[:,0] = node_id_x
 
    f0 = 20.0*np.ones((node_id_x.size,1), dtype = np.float64).ravel()
    
    pulse_type = []
    for isrc in np.arange(node_id_x.size):
        pulse_type.append('ricker')

    source = {'type': pulse_type, 'num_src': node_id_x.size, 'node_id': node_id, 'params': f0 }
    
    
    # receiver
    # type: interior , surf, coord
#    node_id = np.array([[nx/8,0], [nx/2,0], [7*nx/8,0]], dtype = np.int32)
    node_id_x = np.arange(3,nx+1, 4) 
    rec_loc = node_id_x
#    node_id_x = np.array([2*nx/10]) 
    node_id = np.zeros( (node_id_x.size,2), dtype = np.int32)
    node_id[:,0] = node_id_x
    rec_src = Imaging.get_receiver(src_loc = src_loc, rec_loc = rec_loc, min_space = 0, max_space = nx+1, location = 'both') 
    receiver = {'interior': True, 'surface': True, 'point': True, 'num_rec': node_id_x.size, 'node_id': node_id, 'rec_src': rec_src}

    # velocity model 
    # (background type: interp, layer, or nodal,  interpolation type: linear, cubic, spline, or none )
    # (rough type: none, time, or depth, interpolation type: linear, cubic, spline)
        
    # background velocity
    num_params = np.array([nx+1,nz+1], dtype = np.int32)
    value = np.ones((num_params[0]*num_params[1],1))
#    value = np.random.rand(num_params[0]*num_params[1],1)
    """
    value = np.ones((num_params[1],num_params[0]), dtype = np.float64)
    value[nz/2::,:] = 2.0
    #value[nz/2:,:] = 1.2
    #value[3*nz/4:,:] = 1.2    
    #value[nz/2:3*nz/4,:] = 1.2
    #value[3*nz/4::,:] = 1.5
    #B = np.zeros( (nz/4,nx/2), dtype = np.float64)
    
#    value[nz/2:nz/2+2, nx/2-nx/5:nx/2+nx/5+1] = 100.0
    
    B = np.zeros( (2*nz/8,2*nx/8), dtype = np.float64)
    i1 = np.arange(0,B.shape[0])
    j1 = np.arange(0,B.shape[1])
    B[i1,j1] = 1.0
    i2 = np.arange(1,B.shape[0])
    j2 = np.arange(0,B.shape[1]-1)
    B[i2,j2] = 1.0
    i3 = np.arange(0,B.shape[0]-1)
    j3 = np.arange(1,B.shape[1])
    B[i3,j3] = 1.0
    """
    #value[nz/2-nx/5:nz/2+nz/5+1, nx/2-nx/5:nx/2+nx/5+1] += 99.0 * np.rot90(B)
    #value[3*nz/4-nx/8:3*nz/4+nz/8+1, 3*nx/4-nx/8:3*nx/4+nx/8+1] += 100.0 * np.rot90(B)
    #value[nz/2:nz/2+2, :] = 100.0
    #ind = np.triu_indices(nz/4)
    #B[ind] = 1.0
    #value[nz/2:3*nz/4, nx/2+1:] += np.rot90(0.3*B,3)
    
    #value[nz/2,nx/2] = 100.0
    #value = value.reshape((num_params[0]*num_params[1],1))
    #value = np.ones((num_params[0]*num_params[1],1), dtype = np.float64)
    #value[((nx+1)*(nz+1)+1)/2,0] = 2
    #x = np.linspace(xmin+dx/2,xmax-dx/2, num = num_params[0])
    #z = np.linspace(zmin+dz/2,zmax-dz/2, num = num_params[1])
    #x = np.linspace(xmin,xmax, num = num_params[0])
    #z = np.linspace(zmin,zmax, num = num_params[1])

    x, z = np.meshgrid(x,z)
    coord = np.zeros((num_params[0]*num_params[1],2), dtype = np.float64)
    coord[:,0] = x.reshape((1,num_params[0]*num_params[1]))
    coord[:,1] = z.reshape((1,num_params[0]*num_params[1]))
    
    #back_vel_file = foldername + 'back_vel_' + filename + '.mat'
    #data = scipy.io.loadmat(back_vel_file)               
    data = {'value':value , 'coord':coord}
                                    
    background = {'type': 'nodal', 'interp_type': 'none', 'num_params': num_params, 'value': data['value'], 'coord': data['coord']} 
    
    """
    num_params = np.array([10,10], dtype = np.int32)
    value = np.random.rand(num_params[0]*num_params[1],1)
    x = np.linspace(xmin,xmax, num = num_params[0])
    z = np.linspace(zmin,zmax, num = num_params[1])    
    x, z = np.meshgrid(x,z)
    coord = np.zeros((num_params[0]*num_params[1],2), dtype = np.float64)
    coord[:,0] = x.reshape((1,num_params[0]*num_params[1]))
    coord[:,1] = z.reshape((1,num_params[0]*num_params[1]))
        
    background = {'type': 'interp', 'interp_type': 'linear', 'num_params': num_params, 'value': value, 'coord': coord}
    """ 
    
    # rough velocity
    rough = {'type': 'none'}
    
    velocity_model = {'background': background, 'rough': rough}

    # one-way properties
    one_way_data = {'slab_thickness': 1}    
    
    # enitre data
    input_data = {'mesh': mesh, 'boundary': boundary, 'abc': abc, 'velocity_model': velocity_model, 'source': source, 'receiver': receiver, 'one_way_data': one_way_data}
    
    return input_data
    