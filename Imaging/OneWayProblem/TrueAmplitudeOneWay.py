class OneWayTrueAmplitudeSolver:
    def __init__(self, input_data):
        import Imaging
        import numpy as np
        
        self.top_mesh  = Imaging.OneWayMesh(input_data, mesh_type = 'true-amplitude', mesh_loc = 'top') # mesh for first step in sweeping
        self.top_mesh.mesh_generator() # generate the mesh             
        self.step_mesh = Imaging.OneWayMesh(input_data, mesh_type = 'true-amplitude', mesh_loc = 'step') # mesh for all steps except first and and final in sweeping
        self.step_mesh.mesh_generator() # generate the mesh
        self.bottom_mesh = Imaging.OneWayMesh(input_data, mesh_type = 'true-amplitude', mesh_loc = 'bottom') # mesh for last step in sweeping                     
        self.bottom_mesh.mesh_generator() # generate the mesh
        self.full_mesh = Imaging.FullWaveMesh(input_data) # mesh corresbonding to entire domain
        self.full_mesh.mesh_generator(is_one_way = True) # generate the mesh

        self.domain = input_data
        
        self.source = [] 
        for isrc in np.arange(input_data['source']['num_src']):
            pulse_type = input_data['source']['type'][isrc]
            if pulse_type == 'ricker':
                f0 = input_data['source']['params'][isrc]
                source = Imaging.ricker_pulse(input_data, f0)
                self.source.append(source)
            elif pulse_type == 'ricker-centered':
                f0 = input_data['source']['params'][isrc]
                source = Imaging.ricker_pulse_centered(input_data, f0)
                self.source.append(source)                                
            else:
                raise Exception("Undefined pulse type!")
                
        # slab thickness                   
                         
        nz = self.domain['mesh']['ver']['nz']
        typical_thickness = self.domain['one_way_data']['slab_thickness']
        self.num_step = nz/typical_thickness
                             
        slab_thickness = typical_thickness * np.ones( (1,self.num_step), dtype = np.int32).ravel()
        if nz % typical_thickness != 0:
             self.slab_thickness = np.insert(slab_thickness, self.num_step, nz % typical_thickness) 
             self.num_step += 1
        else:
            self.slab_thickness = slab_thickness
     
    def run_analysis(self, problem_type, velocity_model, **migration_data):
        import Imaging
        import numpy as np
        import scipy.sparse as spr
        import scipy.sparse.linalg as linalg
         
        num_src = self.domain['source']['num_src']
        num_rec = self.domain['receiver']['num_rec']
        
        if problem_type == 'Migration':
            num_rhs = 2 * num_src # one for each source + one surface records corresponding to each source
            if 'receiver' in migration_data:
                receiver_pulse = migration_data['receiver']
            else:
                raise Exception('no migration data found')
            if 'freq_ind' in  migration_data:
                  freq_ind = migration_data['freq_ind']
                  num_freq = freq_ind.size
            else:
                raise Exception('no migration frequency found')

        elif problem_type == 'Inversion':
            num_rhs = num_src + num_rec
            freq_ind = self.domain['mesh']['freq']['ind']
            num_freq = freq_ind.size            
        
        elif problem_type == 'Forward':
            num_rhs = num_src
            freq_ind = self.domain['mesh']['freq']['ind']
            num_freq = freq_ind.size            
        else:
            raise Exception("Undefined Problem Type!") 
        
        # initialize the output
        int_disp = []
        surf_disp = []
        rec_disp = []
        
        if self.domain['receiver']['interior'] == True:
            int_disp = np.zeros((self.full_mesh.int_nod.size,num_rhs,num_freq), dtype = np.complex128)
        
        if self.domain['receiver']['surface'] == True:
            surf_disp = np.zeros((self.full_mesh.surf_nod.size,num_rhs,num_freq), dtype = np.complex128)
        
        if self.domain['receiver']['point'] == True:
            rec_disp = np.zeros((self.full_mesh.rec_nod.size,num_rhs,num_freq), dtype = np.complex128)
        
        # main calculation
               
        jw = 0
        
        for iw in freq_ind:
            
            omega = self.domain['mesh']['freq']['w'][iw]
            print 'Frequency = ', omega/(2*np.pi)
            
            # enitre displacement field and force
            u = np.zeros((self.full_mesh.n_node,num_rhs), dtype = np.complex128) # enitre wave field
            
            # sweeping for goes here
            
            down = np.zeros((self.full_mesh.n_node,num_rhs), dtype = np.complex128) # downward  wave field
            r = np.zeros((self.full_mesh.n_node,num_rhs), dtype = np.complex128) # preimary reflected wave field
            f = np.zeros((self.full_mesh.n_node,num_rhs), dtype = np.complex128) # force field
            
            if problem_type == 'Migration':
                for irhs in np.arange(num_rhs):
                    if irhs < num_src:
                        f[self.full_mesh.src_nod[irhs],irhs] = 1.0 * self.source[irhs].get_value_freq(iw)
                        pass
                    else:
                        rec_nod = self.full_mesh.rec_nod[self.domain['receiver']['rec_src'][irhs-num_src]]
                        f[rec_nod,irhs] = receiver_pulse[self.domain['receiver']['rec_src'][irhs-num_src],irhs-num_src,jw]
#                        f[self.full_mesh.rec_nod,irhs] = receiver_pulse[:,irhs-num_src,jw]

                        pass
        
            elif problem_type == 'Inversion':
        
                for irhs in np.arange(num_rhs):
                    if irhs < num_src:
                        f[self.full_mesh.src_nod[irhs],irhs] = 1.0 * self.source[irhs].get_value_freq(iw)
                        pass
                    else:
                        f[self.full_mesh.rec_nod[irhs-num_src],irhs] = 1.0
                        pass

            elif problem_type == 'Forward':
        
                for isrc in np.arange(num_src):
                    f[self.full_mesh.src_nod[isrc], isrc] = 1.0 * self.source[isrc].get_value_freq(iw) 
                    pass               
    
            else:
                raise Exception("Undefined Problem Type!")
            
            
            # downward sweeping
            z0 = 0.0
            iz0 = 0
            #print 'total link = ', self.full_mesh.one_way_link
            
            sweeping_dir = 'downward'
            print 'sweeping direction = ' , sweeping_dir
            
            for istep in np.arange(self.num_step):#np.arange(1): 
                print 'depth = ', z0
               
                if istep == 0:
                    current_mesh = self.top_mesh
                    mesh_loc = 'top'
                    # node link
                    int_link = self.full_mesh.one_way_link[0:self.full_mesh.one_way_abc['top']+self.slab_thickness[0]+1,:].copy()
                    shape = int_link.shape
                    int_link = int_link.reshape( (1, shape[0] * shape[1]) , order = 'F').ravel() 
                    top_link = self.full_mesh.one_way_link[self.full_mesh.one_way_abc['top'],:].copy().ravel()
                    bottom_link = self.full_mesh.one_way_link[self.full_mesh.one_way_abc['top']+self.slab_thickness[0],:].copy().ravel()
                    
                elif istep == self.num_step - 1:
                    current_mesh = self.bottom_mesh
                    mesh_loc = 'bottom'
                    # node link
                    iz = iz0 + self.full_mesh.one_way_abc['top']
                    int_link = self.full_mesh.one_way_link[iz:,:].copy()
                    shape = int_link.shape
                    int_link = int_link.reshape( (1, shape[0] * shape[1]) , order = 'F') .ravel()                  
                    top_link = self.full_mesh.one_way_link[iz,:].copy().ravel()
                    bottom_link = self.full_mesh.one_way_link[iz+self.slab_thickness[istep],:].copy().ravel()
                    
                else:
                    current_mesh = self.step_mesh
                    mesh_loc = 'step'
                    # node link
                    iz = iz0 + self.full_mesh.one_way_abc['top']
                    int_link = self.full_mesh.one_way_link[iz:iz+self.slab_thickness[istep]+1,:].copy()
                    shape = int_link.shape
                    int_link = int_link.reshape( (1, shape[0] * shape[1]) , order = 'F') .ravel()                      
                    top_link = self.full_mesh.one_way_link[iz,:].copy().ravel()
                    bottom_link = self.full_mesh.one_way_link[iz+self.slab_thickness[istep],:].copy().ravel()  
                #print 'int link = ' , int_link
                #print 'surf link = ' , top_link
                #print 'bottom link = ' , bottom_link                                
                # initialize element matrices

                current_mesh.get_matrix(freq = omega, velocity_model = velocity_model, depth = z0,\
                                        is_true_amp = True, direction = sweeping_dir, location = mesh_loc)             
               
                # setting lhs and assembling the matrix
                u_step = np.zeros((current_mesh.n_node,num_rhs), dtype = np.complex128)
                
                A = spr.csc_matrix( ((current_mesh.elem_stiff - (omega**2) * current_mesh.elem_mass).ravel()  , current_mesh.ij_spr_ind ), shape = (current_mesh.n_node,current_mesh.n_node) )
                
                # seting rhs
                f_step = np.zeros((current_mesh.n_node,num_rhs), dtype = np.complex128)
                
                f_step[current_mesh.int_nod,:] = f[int_link,:]

                if istep > 0:
                    f_step[current_mesh.surf_nod,:] = 0.0 + 0.0j
                    u_step[current_mesh.surf_nod,:] = down[top_link,:]
                    A_top = spr.csc_matrix( ((current_mesh.elem_stiff[0:current_mesh.last_top_elem,:] - (omega**2) * current_mesh.elem_mass[0:current_mesh.last_top_elem,:]).ravel()  , current_mesh.ij_spr_ind_top ), shape = (current_mesh.n_node,current_mesh.n_node) )
                    f_step += 2*A_top.dot(u_step)
  
                A = A[current_mesh.active_nod,:]
                A = A[:,current_mesh.active_nod]
                       
                # solving linear system
                if num_rhs > 1:
                    u_step[current_mesh.active_nod,:] = linalg.spsolve(A,spr.csc_matrix(f_step[current_mesh.active_nod,:])).todense()
                else:
                    u_step[current_mesh.active_nod,:] = linalg.spsolve(A,spr.csc_matrix(f_step[current_mesh.active_nod,:]))[:,np.newaxis]
                                        
                r[top_link,:] = u_step[current_mesh.surf_nod,:]-down[top_link,:]
                u_step[current_mesh.surf_nod,:] = down[top_link,:]
                down[int_link,:] = u_step[current_mesh.int_nod,:]

                z0 += self.slab_thickness[istep]*self.domain['mesh']['ver']['dz']
                iz0 += self.slab_thickness[istep]    

            u = down + r
           
            # storing the data    
            if self.domain['receiver']['interior'] == True:
                int_disp[:,:,jw] = u[self.full_mesh.int_nod,:]
                
            if self.domain['receiver']['surface'] == True:
                surf_disp[:,:,jw] = u[self.full_mesh.surf_nod,:]
                
            if self.domain['receiver']['point'] == True:
                rec_disp[:,:,jw] = u[self.full_mesh.rec_nod,:]            
            
            # next frequency        
            jw += 1
        
        # return output as a dictionary        
        output = {'interior': int_disp, 'surface': surf_disp, 'point': rec_disp}
        return output
        
            
     
    def run_analysis_2sweep(self, problem_type, velocity_model, **migration_data):
        import Imaging
        import numpy as np
        import scipy.sparse as spr
        import scipy.sparse.linalg as linalg
         
        num_src = self.domain['source']['num_src']
        num_rec = self.domain['receiver']['num_rec']
        
        if problem_type == 'Migration':
            num_rhs = 2 * num_src # one for each source + one surface records corresponding to each source
            if 'receiver' in migration_data:
                receiver_pulse = migration_data['receiver']
            else:
                raise Exception('no migration data found')
            if 'freq_ind' in  migration_data:
                  freq_ind = migration_data['freq_ind']
                  num_freq = freq_ind.size
            else:
                raise Exception('no migration frequency found')

        elif problem_type == 'Inversion':
            num_rhs = num_src + num_rec
            freq_ind = self.domain['mesh']['freq']['ind']
            num_freq = freq_ind.size            
        
        elif problem_type == 'Forward':
            num_rhs = num_src
            freq_ind = self.domain['mesh']['freq']['ind']
            num_freq = freq_ind.size            
        else:
            raise Exception("Undefined Problem Type!") 
            
        
        # initialize the output
        int_disp = []
        surf_disp = []
        rec_disp = []
        
        if self.domain['receiver']['interior'] == True:
            int_disp = np.zeros((self.full_mesh.int_nod.size,num_rhs,num_freq), dtype = np.complex128)
        
        if self.domain['receiver']['surface'] == True:
            surf_disp = np.zeros((self.full_mesh.surf_nod.size,num_rhs,num_freq), dtype = np.complex128)
        
        if self.domain['receiver']['point'] == True:
            rec_disp = np.zeros((self.full_mesh.rec_nod.size,num_rhs,num_freq), dtype = np.complex128)
        
        # main calculation
               
        jw = 0
        
        for iw in freq_ind:
            
            omega = self.domain['mesh']['freq']['w'][iw]
            print 'Frequency = ', omega/(2*np.pi)
            
            # enitre displacement field and force
            u = np.zeros((self.full_mesh.n_node,num_rhs), dtype = np.complex128) # enitre wave field
            
            # sweeping for goes here
            
            down = np.zeros((self.full_mesh.n_node,num_rhs), dtype = np.complex128) # downward  wave field
            up = np.zeros((self.full_mesh.n_node,num_rhs), dtype = np.complex128) # upward reflected wave field
            r = np.zeros((self.full_mesh.n_node,num_rhs), dtype = np.complex128) # preimary reflected wave field
            q = np.zeros((self.full_mesh.n_node,num_rhs), dtype = np.complex128) # secondary reflected wave field
            f = np.zeros((self.full_mesh.n_node,num_rhs), dtype = np.complex128) # force field
            
            if problem_type == 'Migration':
                for irhs in np.arange(num_rhs):
                    if irhs < num_src:
                        f[self.full_mesh.src_nod[irhs],irhs] = 1.0 * self.source[irhs].get_value_freq(iw)
                        pass
                    else:
                        rec_nod = self.full_mesh.rec_nod[self.domain['receiver']['rec_src'][irhs-num_src]]
                        f[rec_nod,irhs] = receiver_pulse[self.domain['receiver']['rec_src'][irhs-num_src],irhs-num_src,jw]
#                        f[self.full_mesh.rec_nod,irhs] = receiver_pulse[:,irhs-num_src,jw]

                        pass
        
            elif problem_type == 'Inversion':
        
                for irhs in np.arange(num_rhs):
                    if irhs < num_src:
                        f[self.full_mesh.src_nod[irhs],irhs] = 1.0 * self.source[irhs].get_value_freq(iw)
                        pass
                    else:
                        f[self.full_mesh.rec_nod[irhs-num_src],irhs] = 1.0
                        pass

            elif problem_type == 'Forward':
        
                for isrc in np.arange(num_src):
                    f[self.full_mesh.src_nod[isrc], isrc] = 1.0 * self.source[isrc].get_value_freq(iw) 
                    pass               
    
            else:
                raise Exception("Undefined Problem Type!")
            
            
            # downward sweeping
            z0 = 0.0
            iz0 = 0
            #print 'total link = ', self.full_mesh.one_way_link
            
            sweeping_dir = 'downward'
            print 'sweeping direction = ' , sweeping_dir
            
            for istep in np.arange(self.num_step):#np.arange(1): 
                print 'depth = ', z0
               
                if istep == 0:
                    current_mesh = self.top_mesh
                    mesh_loc = 'top'
                    # node link
                    int_link = self.full_mesh.one_way_link[0:self.full_mesh.one_way_abc['top']+self.slab_thickness[0]+1,:].copy()
                    shape = int_link.shape
                    int_link = int_link.reshape( (1, shape[0] * shape[1]) , order = 'F').ravel() 
                    top_link = self.full_mesh.one_way_link[self.full_mesh.one_way_abc['top'],:].copy().ravel()
                    bottom_link = self.full_mesh.one_way_link[self.full_mesh.one_way_abc['top']+self.slab_thickness[0],:].copy().ravel()
                    
                elif istep == self.num_step - 1:
                    current_mesh = self.bottom_mesh
                    mesh_loc = 'bottom'
                    # node link
                    iz = iz0 + self.full_mesh.one_way_abc['top']
                    int_link = self.full_mesh.one_way_link[iz:,:].copy()
                    shape = int_link.shape
                    int_link = int_link.reshape( (1, shape[0] * shape[1]) , order = 'F') .ravel()                  
                    top_link = self.full_mesh.one_way_link[iz,:].copy().ravel()
                    bottom_link = self.full_mesh.one_way_link[iz+self.slab_thickness[istep],:].copy().ravel()
                    
                else:
                    current_mesh = self.step_mesh
                    mesh_loc = 'step'
                    # node link
                    iz = iz0 + self.full_mesh.one_way_abc['top']
                    int_link = self.full_mesh.one_way_link[iz:iz+self.slab_thickness[istep]+1,:].copy()
                    shape = int_link.shape
                    int_link = int_link.reshape( (1, shape[0] * shape[1]) , order = 'F') .ravel()                      
                    top_link = self.full_mesh.one_way_link[iz,:].copy().ravel()
                    bottom_link = self.full_mesh.one_way_link[iz+self.slab_thickness[istep],:].copy().ravel()  
                #print 'int link = ' , int_link
                #print 'surf link = ' , top_link
                #print 'bottom link = ' , bottom_link                                
                # initialize element matrices

                current_mesh.get_matrix(freq = omega, velocity_model = velocity_model, depth = z0,\
                                        is_true_amp = True, direction = sweeping_dir, location = mesh_loc)             
               
                # setting lhs and assembling the matrix
                u_step = np.zeros((current_mesh.n_node,num_rhs), dtype = np.complex128)
                
                A = spr.csc_matrix( ((current_mesh.elem_stiff - (omega**2) * current_mesh.elem_mass).ravel()  , current_mesh.ij_spr_ind ), shape = (current_mesh.n_node,current_mesh.n_node) )
                
                # seting rhs
                f_step = np.zeros((current_mesh.n_node,num_rhs), dtype = np.complex128)
                
                f_step[current_mesh.int_nod,:] = f[int_link,:]

                if istep > 0:
                    f_step[current_mesh.surf_nod,:] = 0.0 + 0.0j
                    u_step[current_mesh.surf_nod,:] = down[top_link,:]+q[top_link,:]
                    A_top = spr.csc_matrix( ((current_mesh.elem_stiff[0:current_mesh.last_top_elem,:] - (omega**2) * current_mesh.elem_mass[0:current_mesh.last_top_elem,:]).ravel()  , current_mesh.ij_spr_ind_top ), shape = (current_mesh.n_node,current_mesh.n_node) )
                    f_step += 2*A_top.dot(u_step)
  
                A = A[current_mesh.active_nod,:]
                A = A[:,current_mesh.active_nod]
                       
                # solving linear system
                if num_rhs > 1:
                    u_step[current_mesh.active_nod,:] = linalg.spsolve(A,spr.csc_matrix(f_step[current_mesh.active_nod,:])).todense()
                else:
                    u_step[current_mesh.active_nod,:] = linalg.spsolve(A,spr.csc_matrix(f_step[current_mesh.active_nod,:]))[:,np.newaxis]                    
                    
                r[top_link,:] = u_step[current_mesh.surf_nod,:]-(down[top_link,:]+q[top_link,:])
                u_step[current_mesh.surf_nod,:] = down[top_link,:]
                down[int_link,:] = u_step[current_mesh.int_nod,:]

                z0 += self.slab_thickness[istep]*self.domain['mesh']['ver']['dz']
                iz0 += self.slab_thickness[istep]    

            # upward sweeping
            sweeping_dir = 'upward'
            print 'sweeping direction = ' , sweeping_dir
            
            z0 -= self.slab_thickness[istep]*self.domain['mesh']['ver']['dz']
            iz0 -= self.slab_thickness[istep] 
            
            upward_range = np.flipud(np.arange(self.num_step-1))

            for istep in upward_range:
                z0 -= self.slab_thickness[istep]*self.domain['mesh']['ver']['dz']
                iz0 -= self.slab_thickness[istep]                 
                
                print 'depth = ', z0
                
                if istep == 0:
                    current_mesh = self.top_mesh
                    mesh_loc = 'top'
                    # node link
                    int_link = self.full_mesh.one_way_link[0:self.full_mesh.one_way_abc['top']+self.slab_thickness[0]+1,:].copy()
                    shape = int_link.shape
                    int_link = int_link.reshape( (1, shape[0] * shape[1]) , order = 'F').ravel() 
                    top_link = self.full_mesh.one_way_link[self.full_mesh.one_way_abc['top'],:].copy().ravel()
                    bottom_link = self.full_mesh.one_way_link[self.full_mesh.one_way_abc['top']+self.slab_thickness[0],:].copy().ravel()
                    
                elif istep == self.num_step - 1:
                    current_mesh = self.bottom_mesh
                    mesh_loc = 'bottom'
                    # node link
                    iz = iz0 + self.full_mesh.one_way_abc['top']
                    int_link = self.full_mesh.one_way_link[iz:,:].copy()
                    shape = int_link.shape
                    int_link = int_link.reshape( (1, shape[0] * shape[1]) , order = 'F') .ravel()                  
                    top_link = self.full_mesh.one_way_link[iz,:].copy().ravel()
                    bottom_link = self.full_mesh.one_way_link[iz+self.slab_thickness[istep],:].copy().ravel()
                    
                else:
                    current_mesh = self.step_mesh
                    mesh_loc = 'step'
                    # node link
                    iz = iz0 + self.full_mesh.one_way_abc['top']
                    int_link = self.full_mesh.one_way_link[iz:iz+self.slab_thickness[istep]+1,:].copy()
                    shape = int_link.shape
                    int_link = int_link.reshape( (1, shape[0] * shape[1]) , order = 'F') .ravel()                      
                    top_link = self.full_mesh.one_way_link[iz,:].copy().ravel()
                    bottom_link = self.full_mesh.one_way_link[iz+self.slab_thickness[istep],:].copy().ravel()  
                #print int_link
                #print top_link
                #print bottom_link
                # current mesh    
                current_mesh.get_matrix(freq = omega, velocity_model = velocity_model, depth = z0,\
                                        is_true_amp = True, direction = sweeping_dir, location = mesh_loc)             
                
                # setting lhs and assembling the matrix
                u_step = np.zeros((current_mesh.n_node,num_rhs), dtype = np.complex128)
                
                A = spr.csc_matrix( ((current_mesh.elem_stiff - (omega**2) * current_mesh.elem_mass).ravel()  , current_mesh.ij_spr_ind ), shape = (current_mesh.n_node,current_mesh.n_node) )
                                
                # seting rhs
                f_step = np.zeros((current_mesh.n_node,num_rhs), dtype = np.complex128)
                u_step[current_mesh.bottom_nod,:] = up[bottom_link,:]+r[bottom_link,:]
                A_bottom = spr.csc_matrix( ((current_mesh.elem_stiff[current_mesh.first_bottom_elem:,:] - (omega**2) * current_mesh.elem_mass[current_mesh.first_bottom_elem:,:]).ravel()  , current_mesh.ij_spr_ind_bottom ), shape = (current_mesh.n_node,current_mesh.n_node) )
                f_step = 2*A_bottom.dot(u_step)
 
                A = A[current_mesh.active_nod,:]
                A = A[:,current_mesh.active_nod]
                       
                # solving linear system
                if num_rhs > 1:
                    u_step[current_mesh.active_nod,:] = linalg.spsolve(A,spr.csc_matrix(f_step[current_mesh.active_nod,:])).todense()
                else:
                    u_step[current_mesh.active_nod,:] = linalg.spsolve(A,spr.csc_matrix(f_step[current_mesh.active_nod,:]))[:,np.newaxis]
                                        
                q[bottom_link,:] = u_step[current_mesh.bottom_nod,:]-(up[bottom_link,:]+r[bottom_link,:])
                u_step[current_mesh.bottom_nod,:] = up[bottom_link,:]
                up[int_link,:] = u_step[current_mesh.int_nod,:]
                                
            u = down + up + q + r
           
            # storing the data    
            if self.domain['receiver']['interior'] == True:
                int_disp[:,:,jw] = u[self.full_mesh.int_nod,:]
                
            if self.domain['receiver']['surface'] == True:
                surf_disp[:,:,jw] = u[self.full_mesh.surf_nod,:]
                
            if self.domain['receiver']['point'] == True:
                rec_disp[:,:,jw] = u[self.full_mesh.rec_nod,:]            
            
            # next frequency        
            jw += 1
        
        # return output as a dictionary        
        output = {'interior': int_disp, 'surface': surf_disp, 'point': rec_disp}
        return output
        
