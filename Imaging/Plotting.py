# plotting functions

def plot_mesh(mesh):
    import matplotlib.pyplot as plt
    import numpy as np
    
    color_list = ['b-','g-','r-','c-','m-','y-','k-','b--','k--']
    
    hf = plt.figure()
    hf.add_subplot(1,1,1)
    """
    # plot nodes
    hf.axes[0].plot(mesh.nod_coord[:,0],mesh.nod_coord[:,1],'ro')
    """
    # plot interior nodes
    #hf.axes[0].plot(mesh.nod_coord[mesh.active_nod_migration,0],mesh.nod_coord[mesh.active_nod_migration,1],'bx')
    hf.axes[0].plot(mesh.nod_coord[mesh.active_nod,0],mesh.nod_coord[mesh.active_nod,1],'ro')
    
    # plot surface nodes
    #hf.axes[0].plot(mesh.nod_coord[mesh.surf_nod,0],mesh.nod_coord[mesh.surf_nod,1],'r.')        
    
    # plot bottom nodes
    
    #if 'bottom_nod' in dir(mesh):
     #   hf.axes[0].plot(mesh.nod_coord[mesh.bottom_nod,0],mesh.nod_coord[mesh.bottom_nod,1],'bo') 

    # plot sources
    if 'src_nod' in dir(mesh):
        hf.axes[0].plot(mesh.nod_coord[mesh.src_nod,0],mesh.nod_coord[mesh.src_nod,1],'k*')  
              
    # plot reciecers
    if 'rec_nod' in dir(mesh):
        hf.axes[0].plot(mesh.nod_coord[mesh.rec_nod,0],mesh.nod_coord[mesh.rec_nod,1],'kv')            
           
    n_elemset = len(mesh.elemset)
    for iset in np.arange(n_elemset):
        for ielem in np.arange(mesh.elemset[iset]['n_elem']):
            coord = np.zeros((mesh.elem_nod+1,2))
            coord[0:mesh.elem_nod,:] = mesh.nod_coord[mesh.elemset[iset]['conn'][ielem],:]
            coord[-1,:] = coord[0,:]
            hf.axes[0].plot(coord[:,0],coord[:,1],color_list[iset])
            
            
    hf.axes[0].set_xlim(mesh.nod_coord[:,0].min()-1,mesh.nod_coord[:,0].max()+1)
    hf.axes[0].set_ylim(mesh.nod_coord[:,1].min()-1,mesh.nod_coord[:,1].max()+1)

    hf.axes[0].invert_yaxis()
    hf.axes[0].set_xlabel('x')
    hf.axes[0].set_ylabel('z')
    hf.axes[0].set_title('mesh') 
    hf.show()
    pass
    
def plot_velocity(velocity_model,input_data):
    import matplotlib.pyplot as plt
    import numpy as np
    
    
    x = input_data['mesh']['hor']['x']
    z = input_data['mesh']['ver']['z']
    
    nx = input_data['mesh']['hor']['nx']
    nz = input_data['mesh']['ver']['nz']
    
    x, z = np.meshgrid(x,z)
    
    rep_coord = np.zeros(((nx+1)*(nz+1),2))
    rep_coord[:,0] = x.reshape( (1,(nx+1)*(nz+1)) )
    rep_coord[:,1] = z.reshape( (1,(nx+1)*(nz+1)) ) 
    
    interp_vel = velocity_model.get_velocity(rep_coord)
    
    interp_vel = interp_vel.reshape(nz+1, nx+1)
    
    
    hf = plt.figure()
    hf.add_subplot(1,1,1)
    #hc = hf.axes[0].contourf(x, z, interp_vel)
    #hc.set_clim(0,5*interp_vel.min())
    #hb = hf.colorbar(hc)
    hc = hf.axes[0].pcolor(x, z, interp_vel, cmap = plt.cm.bone, vmax = 5*np.abs(interp_vel).min(), vmin = np.abs(interp_vel).min())
    hf.colorbar(hc)
    
    hf.axes[0].invert_yaxis()
    hf.axes[0].set_xlabel('x')
    hf.axes[0].set_ylabel('z')
    hf.axes[0].set_title('velocity')   
    hf.show()
    
    pass
    
def plot_int_disp(disp, input_data, **plot_data):
    # plot interior displacement 
    from mpl_toolkits.mplot3d import Axes3D
    import matplotlib.pyplot as plt
    import numpy as np
    
    freq_ind = plot_data['freq_no']-1
    src_ind = plot_data['source_no']-1
    
    x = input_data['mesh']['hor']['x']
    z = input_data['mesh']['ver']['z']
    
    nx = input_data['mesh']['hor']['nx']
    nz = input_data['mesh']['ver']['nz']
    
    x, z = np.meshgrid(x,z)
    print disp['interior'].size
    data = disp['interior'][:,src_ind,freq_ind].reshape( (nz+1,nx+1), order = 'F')
    
    hf = plt.figure()
    hf.add_subplot(1,1,1)
    hc = hf.axes[0].contourf(x, z, data.real)
    hf.colorbar(hc)
    hf.axes[0].invert_yaxis()
    hf.axes[0].set_xlabel('x')
    hf.axes[0].set_ylabel('z')
    hf.axes[0].set_title('interior disp: real part') 
    hf.show()
       
    hf = plt.figure()
    hf.add_subplot(1,1,1)
    hc = hf.axes[0].contourf(x, z, data.imag)
    hf.colorbar(hc)
    hf.axes[0].invert_yaxis()
    hf.axes[0].set_xlabel('x')
    hf.axes[0].set_ylabel('z')
    hf.axes[0].set_title('interior disp: imaginary part')
    hf.show()                    
    
    pass
    
def plot_surf_disp(disp, input_data, **plot_data):
    # plot interior displacement 
    
    import matplotlib.pyplot as plt
    import numpy as np
    
    freq_ind = plot_data['freq_no']-1
    src_ind = plot_data['source_no']-1
    
    x = input_data['mesh']['hor']['x']
    
    nx = input_data['mesh']['hor']['nx']
    
    data = disp['surface'][:,src_ind,freq_ind]
    
    hf = plt.figure()
    hf.add_subplot(1,1,1)
    hf.axes[0].plot(x, data.real)
    hf.axes[0].set_xlabel('x')
    hf.axes[0].set_ylabel('real part')
    hf.axes[0].set_title('sriface disp') 
    hf.show()
       
    hf = plt.figure()
    hf.add_subplot(1,1,1)
    hf.axes[0].plot(x, data.imag)
    hf.axes[0].set_xlabel('x')
    hf.axes[0].set_ylabel('imaginary part')
    hf.axes[0].set_title('interior disp')
    hf.show()       

def plot_int_error(disp1, disp2, input_data, **plot_data):
    # plot interior displacement 
    from mpl_toolkits.mplot3d import Axes3D
    import matplotlib.pyplot as plt
    import numpy as np
    
    freq_ind = plot_data['freq_no']-1
    src_ind = plot_data['source_no']-1
    
    x = input_data['mesh']['hor']['x']
    z = input_data['mesh']['ver']['z']
    
    nx = input_data['mesh']['hor']['nx']
    nz = input_data['mesh']['ver']['nz']
    
    x, z = np.meshgrid(x,z)
    data1 = disp1['interior'][:,src_ind,freq_ind].reshape( (nz+1,nx+1), order = 'F')
    data2 = disp2['interior'][:,src_ind,freq_ind].reshape( (nz+1,nx+1), order = 'F')
    hf = plt.figure()
    hf.add_subplot(1,1,1)
    hc = hf.axes[0].contourf(x, z, (data1-data2).real)

    hf.colorbar(hc)
    hf.axes[0].invert_yaxis()
    hf.axes[0].set_xlabel('x')
    hf.axes[0].set_ylabel('z')
    hf.axes[0].set_title('error in interior disp: real part') 
    hf.show()
       
    hf = plt.figure()
    hf.add_subplot(1,1,1)
    hc = hf.axes[0].contourf(x, z, (data1-data2).imag)
    hf.colorbar(hc)
    hf.axes[0].invert_yaxis()
    hf.axes[0].set_xlabel('x')
    hf.axes[0].set_ylabel('z')
    hf.axes[0].set_title('error in interior disp: imaginary part')
    hf.show()
    
    print 'norm error = ', np.linalg.norm(data1.ravel()-data2.ravel())/np.linalg.norm(data2.ravel())
    
def plot_image(image, input_data, **plot_data):
    # plot migrated image
    from mpl_toolkits.mplot3d import Axes3D
    import matplotlib.pyplot as plt
    import numpy as np
    from matplotlib.image import NonUniformImage
    title = plot_data['title']
    
    x = input_data['mesh']['hor']['x']
    z = input_data['mesh']['ver']['z']
    
    nx = input_data['mesh']['hor']['nx']
    nz = input_data['mesh']['ver']['nz']
    
    x, z = np.meshgrid(x,z)
    data = image.reshape( (nz+1,nx+1), order = 'F')/np.abs(image).max()
    
#    for isrc in np.arange(input_data['source']['num_src']):
 #       ix = input_data['source']['node_id'][isrc,0]
  #      iz = input_data['source']['node_id'][isrc,1]        
   #     data[iz,ix] = data[ix,iz+1]    
    
    hf = plt.figure()
    hf.add_subplot(1,1,1,projection='3d')
    hc = hf.axes[0].plot_surface(x, z, data)
    #hf.colorbar(hc)
    hf.axes[0].invert_yaxis()
    hf.axes[0].set_xlabel('x')
    hf.axes[0].set_ylabel('z')
    hf.axes[0].set_title('Migrated Image: ' + title) 
    hf.show()
    
    #data[0:15,:] = 0.0 
    
    hf = plt.figure()
    ha = hf.add_subplot(1,1,1)
    #hc = hf.axes[0].contourf(x, z, data, cmap = plt.cm.gray)
#    hc = hf.axes[0].pcolor(x, z, data, cmap = plt.cm.bone, vmax = np.abs(data).max(), vmin = -np.abs(data).max(), shading = 'interp' )
    hc = hf.axes[0].pcolor(x, z, data, cmap = plt.cm.bone, vmax = 1.0, vmin = -1.0, shading = 'interp' )
    # im = NonUniformImage(ha, interpolation = 'nearest', cmap = plt.cm.bone, extent=(x.min(),x.max(),z.min(),z.max()))
    #im.set_data(input_data['mesh']['hor']['x'], input_data['mesh']['ver']['z'], data)
    #hf.axes[0].images.append(im) 
    #hf.axes[0].set_xlim(x.min(),x.max())
    #hf.axes[0].set_ylim(z.min(),z.max())
    hf.axes[0].invert_yaxis()
    hf.axes[0].set_xlabel('x')
    hf.axes[0].set_ylabel('z')
    hf.axes[0].set_title('Migrated Image: ' + title) 
    hf.show()    
    pass    