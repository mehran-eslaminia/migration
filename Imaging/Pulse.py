class ricker_pulse:
    def __init__(self,input_data,params):
        import numpy as np
        import scipy.fftpack as fftpack
        
        f0 = params        
        self.f0 = f0 # centeral frequency
        
        t0 = 1.0/f0
        t = input_data['mesh']['time']['t']

        self.value_time = (1.0 - 2.0 * np.pi**2* f0**2 * (t-t0)**2) * np.exp( -np.pi**2 * f0**2* (t-t0)**2) 
        
        self.value_freq = fftpack.fftshift(fftpack.fft(self.value_time))
        
    def get_value_time(self, ind):
        if ind == 'all':
            return self.value_time
        else:
            return self.value_time[ind]                                    
        
    def get_value_freq(self, ind):
        if ind == 'all':
            return self.value_freq  
        else:
            return self.value_freq[ind]  
        
class ricker_pulse_centered:
    def __init__(self,input_data,params):
        import numpy as np
        import scipy.fftpack as fftpack
        
        f0 = params        
        self.f0 = f0 # centeral frequency
        
        t0 = input_data['mesh']['time']['tmax']/2.0
        t = input_data['mesh']['time']['t']

        self.value_time = fftpack.ifftshift( (1.0 - 2.0 * np.pi**2* f0**2 * (t-t0)**2) * np.exp( -np.pi**2 * f0**2* (t-t0)**2) )
        
        self.value_freq = fftpack.fftshift(fftpack.fft(self.value_time))
        
    def get_value_time(self, ind):
        if ind == 'all':
            return self.value_time
        else:
            return self.value_time[ind]                                    
        
    def get_value_freq(self, ind):
        if ind == 'all':
            return self.value_freq  
        else:
            return self.value_freq[ind] 