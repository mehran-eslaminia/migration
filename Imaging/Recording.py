def get_receiver(**in_data):

    location = in_data['location']
    src_loc = in_data['src_loc']
    rec_loc = in_data['rec_loc']
    min_space = in_data['min_space']
    max_space = in_data['max_space']

    rec_src = []
    import numpy as np

    num_src = src_loc.size

    rec_number = np.arange(rec_loc.size)

    for isrc in np.arange(num_src):

        if location == 'left':
            node_id =( rec_loc <= src_loc[isrc] - min_space) * ( rec_loc >= src_loc[isrc] - max_space)
            node_no = rec_number[node_id].copy()

        elif location == 'right':
            node_id = (rec_loc >= src_loc[isrc] + min_space) * ( rec_loc <= src_loc[isrc] + max_space)
            node_no = rec_number[node_id].copy()

        elif location == 'both':
            node_id = (np.abs(rec_loc - src_loc[isrc]) >= min_space) * (np.abs(rec_loc - src_loc[isrc]) <= max_space)
            node_no = rec_number[node_id].copy()

        else:
            raise Exception('undefined location')

        if node_no.size == 0:
            raise Exception('no receiver')            

        rec_src.append(node_no)

    return rec_src