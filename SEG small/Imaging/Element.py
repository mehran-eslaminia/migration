# element mass stiff matrcies

def get_matrix(elem_type):
    
    import numpy as np
    
    if elem_type == 'center':
        mass = 1.0/(12.0)**2 * np.array([[25,5,1,5],[5,25,5,1],[1,5,25,5],[5,1,5,25]], dtype = np.float64).reshape((1,16))
        stiff1 = 1.0/12.0 * np.array([[5,-5,-1,1],[-5,5,1,-1],[-1,1,5,-5],[1,-1,-5,5]], dtype = np.float64).reshape((1,16))
        stiff2 = 1.0/12.0 * np.array([[5,1,-1,-5],[1,5,-5,-1],[-1,-5,5,1],[-5,-1,1,5]], dtype = np.float64).reshape((1,16))

    elif elem_type == 'side':
        mass = 1.0/(12.0)**2 * np.array([[15,15,3,3],[15,15,3,3],[3,3,15,15],[3,3,15,15]], dtype = np.float64).reshape((1,16))
        stiff1 = 1.0/12.0 * np.array([[5,-5,-1,1],[-5,5,1,-1],[-1,1,5,-5],[1,-1,-5,5]], dtype = np.float64).reshape((1,16))
        stiff2 = 1.0/12.0 * np.array([[3,3,-3,-3],[3,3,-3,-3],[-3,-3,3,3],[-3,-3,3,3]], dtype = np.float64).reshape((1,16))  
        
    elif elem_type == 'top' or elem_type == 'bottom':
        mass = 1.0/(12.0)**2 * np.array([[15,3,3,15],[3,15,15,3],[3,15,15,3],[15,3,3,15]], dtype = np.float64).reshape((1,16))
        stiff1 = 1.0/12.0 * np.array([[3,-3,-3,3],[-3,3,3,-3],[-3,3,3,-3],[3,-3,-3,3]], dtype = np.float64).reshape((1,16))
        stiff2 = 1.0/12.0 * np.array([[5,1,-1,-5],[1,5,-5,-1],[-1,-5,5,1],[-5,-1,1,5]], dtype = np.float64).reshape((1,16))         

    elif elem_type == 'corner':
        mass = 1.0/(12.0)**2 * np.array([[9,9,9,9],[9,9,9,9],[9,9,9,9],[9,9,9,9]], dtype = np.float64).reshape((1,16))
        stiff1 = 1.0/12.0 * np.array([[3,-3,-3,3],[-3,3,3,-3],[-3,3,3,-3],[3,-3,-3,3]], dtype = np.float64).reshape((1,16))
        stiff2 = 1.0/12.0 * np.array([[3,3,-3,-3],[3,3,-3,-3],[-3,-3,3,3],[-3,-3,3,3]], dtype = np.float64).reshape((1,16)) 

    else:
        raise Exception("Undefined element type!")
    
    return mass, stiff1, stiff2
    