class FullWaveDirectSolver:
    
    def __init__(self,input_data):
        import Imaging

        import numpy as np
        
        self.mesh = Imaging.FullWaveMesh(input_data)
        self.mesh.mesh_generator() # generate the mesh
        self.domain = input_data
        self.source = [] 
        for isrc in np.arange(input_data['source']['num_src']):
            pulse_type = input_data['source']['type'][isrc]
            if pulse_type == 'ricker':
                f0 = input_data['source']['params'][isrc]
                source = Imaging.ricker_pulse(input_data, f0)
                self.source.append(source)
            elif pulse_type == 'ricker-centered':
                f0 = input_data['source']['params'][isrc]
                source = Imaging.ricker_pulse_centered(input_data, f0)
                self.source.append(source)                
            else:
                raise Exception("Undefined pulse type!")                                          
                                                                     
        
    def run_analysis(self, problem_type, velocity_model, **migration_data):
        import Imaging
        import numpy as np
        import scipy.sparse as spr
        import scipy.sparse.linalg as linalg
        
        num_src = self.domain['source']['num_src']
        num_rec = self.domain['receiver']['num_rec']

        if problem_type == 'Migration':
            num_rhs = 2 * num_src # one for each source + one surface records corresponding to each source
            if 'receiver' in migration_data:
                receiver_pulse = migration_data['receiver']
            else:
                raise Exception('no migration data found')
            if 'freq_ind' in  migration_data:
                  freq_ind = migration_data['freq_ind']
                  num_freq = freq_ind.size
            else:
                raise Exception('no migration frequency found')
                
        elif problem_type == 'Inversion':
            num_rhs = num_src + num_rec
            freq_ind = self.domain['mesh']['freq']['ind']
            num_freq = freq_ind.size        
        elif problem_type == 'Forward':
            num_rhs = num_src
            freq_ind = self.domain['mesh']['freq']['ind']
            num_freq = freq_ind.size
        else:
            raise Exception("Undefined Problem Type!") 

        # initialize the output
        int_disp = []
        surf_disp = []
        rec_disp = []
        
        if (self.domain['receiver']['interior'] == True) or (problem_type != 'Forward'):
            int_disp = np.zeros((self.mesh.int_nod.size,num_rhs,num_freq), dtype = np.complex128)
        
        if self.domain['receiver']['surface'] == True:
            surf_disp = np.zeros((self.mesh.surf_nod.size,num_rhs,num_freq), dtype = np.complex128)
        
        if (self.domain['receiver']['point'] == True) or (problem_type == 'Inversion'):
            rec_disp = np.zeros((self.mesh.rec_nod.size,num_rhs,num_freq), dtype = np.complex128)
        
        # main calculation
        
        jw = 0
        for iw in freq_ind:
            
            omega = self.domain['mesh']['freq']['w'][iw]
            print 'Frequency = ', omega/(2*np.pi)
            
            self.mesh.get_matrix(freq = omega, velocity_model = velocity_model)
            
            # seting rhs
            f = np.zeros((self.mesh.n_node,num_rhs), dtype = np.complex128)
            
            if problem_type == 'Migration':
                for irhs in np.arange(num_rhs):
                    if irhs < num_src:
                        f[self.mesh.src_nod[irhs],irhs] = 1.0 * self.source[irhs].get_value_freq(iw)
                        
                    else:
                        rec_nod = self.mesh.rec_nod[self.domain['receiver']['rec_src'][irhs-num_src]]
                        f[rec_nod,irhs] = receiver_pulse[self.domain['receiver']['rec_src'][irhs-num_src],irhs-num_src,jw]
#                        f[self.mesh.rec_nod,irhs] = receiver_pulse[:,irhs-num_src,jw]
                        
                
            elif problem_type == 'Inversion':
                
                for irhs in np.arange(num_rhs):
                    if irhs < num_src:
                        f[self.mesh.src_nod[irhs],irhs] = 1.0 * self.source[irhs].get_value_freq(iw)
                        
                    else:
                        f[self.mesh.rec_nod[irhs-num_src],irhs] = 1.0
     
            elif problem_type == 'Forward':
                
                for isrc in np.arange(num_src):
                    f[self.mesh.src_nod[isrc], isrc] = 1.0 * self.source[isrc].get_value_freq(iw)                
            
            else:
                raise Exception("Undefined Problem Type!") 
            
            # seting lhs and assmbeling the matrx
            u = np.zeros((self.mesh.n_node,num_rhs), dtype = np.complex128) 
            """
            mass = spr.csc_matrix( ((self.mesh.elem_mass).ravel()  , self.mesh.ij_spr_ind ), shape = (self.mesh.n_node,self.mesh.n_node) )
            stiff = spr.csc_matrix( ((self.mesh.elem_stiff).ravel()  , self.mesh.ij_spr_ind ), shape = (self.mesh.n_node,self.mesh.n_node) )
            print 'mass = \n' , mass.todense().real
            print 'stiff = \n' , stiff.todense().real
            
            print 'mass = \n' , mass.todense().imag
            print 'stiff = \n' , stiff.todense().imag            
            """

            A = spr.csc_matrix( ((self.mesh.elem_stiff - (omega**2) * self.mesh.elem_mass).ravel()  , self.mesh.ij_spr_ind ), shape = (self.mesh.n_node,self.mesh.n_node) )
            
            A = A[self.mesh.active_nod,:]
            A = A[:,self.mesh.active_nod]
                       
            # solving linear system
            if num_rhs > 1:
                u[self.mesh.active_nod,:] = linalg.spsolve(A,spr.csc_matrix(f[self.mesh.active_nod,:])).todense()
            else:
                u[self.mesh.active_nod,:] = linalg.spsolve(A,spr.csc_matrix(f[self.mesh.active_nod,:]))[:,np.newaxis]
            
            # storing the data    
            if (self.domain['receiver']['interior'] == True) or (problem_type != 'Forward'):
                int_disp[:,:,jw] = u[self.mesh.int_nod,:]
                
            if self.domain['receiver']['surface'] == True:
                surf_disp[:,:,jw] = u[self.mesh.surf_nod,:]
                
            if (self.domain['receiver']['point'] == True) or (problem_type == 'Inversion'):
                rec_disp[:,:,jw] = u[self.mesh.rec_nod,:]

            # next frequency        
            jw += 1
        
        # return output as a dictionary        
        output = {'interior': int_disp, 'surface': surf_disp, 'point': rec_disp}
        return output
    