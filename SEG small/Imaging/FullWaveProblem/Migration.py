class FullWaveMigration:
    def __init__(self,input_data, solver_type):
        import Imaging
        import numpy as np
        import copy
        
        self.domain = input_data
        self.direct_wave_domain =copy.deepcopy(input_data)
        
        self.direct_wave_domain['mesh']['ver']['nz'] = 1
        self.direct_wave_domain['mesh']['ver']['zmax'] = 1*self.direct_wave_domain['mesh']['ver']['dz'] \
        + self.direct_wave_domain['mesh']['ver']['zmin']
        
        self.direct_wave_domain['mesh']['ver']['z'] = np.linspace(self.direct_wave_domain['mesh']['ver']['zmin']\
        ,self.direct_wave_domain['mesh']['ver']['zmax'],num = 2)

        if solver_type == 'direct':
            self.solver = Imaging.FullWaveDirectSolver(self.domain)
            self.direct_wave_solver = Imaging.FullWaveDirectSolver(self.direct_wave_domain)
        else:
            raise Exception('undefined solver')
        self.trace = []
        self.image = []
        self.image1 =[]
        self.image2 = []
        self.image3 = []        
    pass
    
    def get_trace(self,velocity_model,input_trace):
        self.trace = input_trace['point']
        # removing the direct wave goes here
        direct_wave = self.direct_wave_solver.run_analysis('Forward',velocity_model)
        self.trace -= direct_wave['point']        
        pass
        
    def get_image(self,velocity_model):
        import numpy as np
        from mpl_toolkits.mplot3d import Axes3D
        import matplotlib.pyplot as plt  
              
        freq_ind = self.domain['mesh']['freq']['ind']
        num_freq = freq_ind.size
        num_src = self.domain['source']['num_src']
        print num_src
        
        # get data for migration (source + backpropagation of receiver) 
        
        self.image = np.zeros( ((self.domain['mesh']['hor']['nx']+1)*(self.domain['mesh']['ver']['nz']+1)), dtype = np.float64)
#        self.image1 = np.zeros( ((self.domain['mesh']['hor']['nx']+1)*(self.domain['mesh']['ver']['nz']+1)), dtype = np.float64)
#        self.image2 = np.zeros( ((self.domain['mesh']['hor']['nx']+1)*(self.domain['mesh']['ver']['nz']+1)), dtype = np.float64)
#        self.image3 = np.zeros( ((self.domain['mesh']['hor']['nx']+1)*(self.domain['mesh']['ver']['nz']+1)), dtype = np.float64)
        jw = 0
        
        image = np.zeros( ((self.domain['mesh']['hor']['nx']+1)*(self.domain['mesh']['ver']['nz']+1), num_src), dtype = np.float64)
        norm_factor1 = np.zeros( ((self.domain['mesh']['hor']['nx']+1)*(self.domain['mesh']['ver']['nz']+1), num_src), dtype = np.float64)        
        norm_factor2 = np.zeros( ((self.domain['mesh']['hor']['nx']+1)*(self.domain['mesh']['ver']['nz']+1), num_src), dtype = np.float64)        
        
        for iw in freq_ind:
            trace = np.zeros( (self.trace.shape[0],self.trace.shape[1],1) , dtype = np.complex128)
            trace[:,:,0] = self.trace[:,:,jw]
            output = self.solver.run_analysis('Migration',velocity_model, receiver = np.conj(trace), freq_ind = np.array([iw]))
            
            for isrc in np.arange(num_src):
                norm_factor1[:,isrc] += np.real( np.conj(output['interior'][:,isrc,0]) * output['interior'][:,isrc,0] )
                norm_factor2[:,isrc] += np.real( np.conj(output['interior'][:,num_src+isrc,0]) * output['interior'][:,num_src+isrc,0] )                
                image[:,isrc] += np.real( output['interior'][:,isrc,0] * output['interior'][:,num_src+isrc,0] )
                
            jw += 1

 #       self.image1 = np.sum(image, axis = 1)[:,np.newaxis]
 #       self.image2 = np.sum(image/(norm_factor1*norm_factor2), axis = 1)[:,np.newaxis]
 #       self.image3 = np.sum(image/np.sqrt(norm_factor1*norm_factor2), axis = 1)[:,np.newaxis]        
        self.image = np.sum(image/(norm_factor1), axis = 1)[:,np.newaxis]  
        

        
        pass
        
     
           