class FullWaveMesh:
    # mesh data for full wave solver
    def __init__(self,input_data):
        import copy
        self.mesh_data = copy.deepcopy(input_data.copy())
        self.nod_coord = [] # nodal coordinates in horizontal and vertical numbering (integer)
        self.n_node = 0 # number of nodes in entire mesh
        self.elem_nod = 4 # num of nodes in elements
        self.active_nod = [] # active nodes in mesh
        self.active_nod_migration = [] # active nodes in mesh for migration (backpropagation)
        self.elemset = [] # element sets 
        self.surf_nod = [] # surface nodes
        self.src_nod = [] # source nodes
        self.rec_nod = [] # receiver nodes
        self.int_nod = [] # interior nodes
        self.elem_stiff = [] # element matrix: stiffness
        self.elem_mass = [] # element matrix: mass 
        self.ij_spr_ind = [] # index vector for sparse assembling
        self.one_way_link = []
        self.one_way_abc = {}
        self.is_init = False
        
    def mesh_generator(self, **one_way_data):
        # called one time to generate mesh
        import numpy as np
        
        # nodal coordinates
        nx = self.mesh_data['mesh']['hor']['nx']
        nz = self.mesh_data['mesh']['ver']['nz']
        xmin = self.mesh_data['mesh']['hor']['xmin'] 
        xmax = self.mesh_data['mesh']['hor']['xmax'] 
        zmin = self.mesh_data['mesh']['ver']['zmin'] 
        zmax = self.mesh_data['mesh']['ver']['zmax'] 
        dx = self.mesh_data['mesh']['hor']['dx'] 
        dz = self.mesh_data['mesh']['ver']['dz'] 
        
        nabc = {'left':0, 'right':0, 'top':0, 'bottom':0}
        for ibound in self.mesh_data['boundary']:
            if self.mesh_data['boundary'][ibound] == 'abc':
                nabc[ibound] = self.mesh_data['abc'][ibound]['nabc']
          
        self.n_node = (nx + nabc['left'] + nabc['right'] +1)*(nz + nabc['top'] + nabc['bottom'] +1)
        self.nod_coord = np.zeros((self.n_node,2), dtype = np.int32)
        
        nod_id = np.arange(self.n_node).reshape((nz + nabc['top'] + nabc['bottom'] +1, nx + nabc['left'] + nabc['right'] +1), order = 'F') 

        if 'is_one_way' in one_way_data:
            if one_way_data['is_one_way']:
                self.one_way_link = nod_id
                self.one_way_abc = nabc
        
        x = np.arange(nx + nabc['left'] + nabc['right'] +1, dtype = np.int32)
        z = np.arange(nz + nabc['top'] + nabc['bottom'] +1, dtype = np.int32)
        X, Z = np.meshgrid(x,z)
        self.nod_coord[:,0] = X.reshape((1,self.n_node), order = 'F')
        self.nod_coord[:,1] = Z.reshape((1,self.n_node), order = 'F')
                
        # interior node Index
        int_ind = np.zeros((nz + nabc['top'] + nabc['bottom'] +1, nx + nabc['left'] + nabc['right'] +1), dtype = np.bool)
        int_ind[nabc['top']:nz + nabc['top']+1, nabc['left']:nx + nabc['left']+1] = True
        int_nod_id = nod_id[int_ind].reshape(nz+1, nx+1)     

        # interior nodes
        self.int_nod = int_nod_id.reshape(((nx+1)*(nz+1),1), order = 'F').ravel()
                                
        # surface nodes
        self.surf_nod = int_nod_id[0,:].reshape((nx+1,1), order = 'F').ravel()
        
        # source nodes
        indx = self.mesh_data['source']['node_id'][:,0]
        indz = self.mesh_data['source']['node_id'][:,1]
        self.src_nod = int_nod_id[indz, indx].ravel()
        
        # receiver nodes
        indx = self.mesh_data['receiver']['node_id'][:,0]
        indz = self.mesh_data['receiver']['node_id'][:,1]
        self.rec_nod = int_nod_id[indz, indx].ravel()
        
        # active nodes
        active_nod = np.ones((self.n_node,1), dtype = np.bool)
        
        if self.mesh_data['boundary']['left'] != 'free':
            ind = X == 0
            active_nod[ind.reshape((self.n_node,1), order = 'F')] = False
            
        if self.mesh_data['boundary']['right'] != 'free':
            ind = X == nx + nabc['left'] + nabc['right']
            active_nod[ind.reshape((self.n_node,1), order = 'F')] = False
            
        if self.mesh_data['boundary']['top'] != 'free':
            ind = Z == 0
            active_nod[ind.reshape((self.n_node,1), order = 'F')] = False
            
        if self.mesh_data['boundary']['bottom'] != 'free':
            ind = Z == nz + nabc['top'] + nabc['bottom']
            active_nod[ind.reshape((self.n_node,1), order = 'F')] = False

        self.active_nod = nod_id.reshape(( (nz + nabc['top'] + nabc['bottom'] +1)*(nx + nabc['left'] + nabc['right'] +1)), order = 'F')[active_nod.ravel()].copy()
        
        # migration acitve node
        ind = Z == 0 # fix surface
        active_nod[ind.reshape((self.n_node,1), order = 'F')] = False
        
        self.active_nod_migration = nod_id.reshape(( (nz + nabc['top'] + nabc['bottom'] +1)*(nx + nabc['left'] + nabc['right'] +1)), order = 'F')[active_nod.ravel()].copy()
        
        # element sets
         
        ielemset = 0

        # left top corner
        if nabc['left']>0 and nabc['top']>0:
            ielemset += 1
            n_elem =  nabc['left'] * nabc['top']
            elem_type = 'corner' 
      
            # nodal connectivity
            ix_beg = 0
            ix_end = nabc['left']
            iz_beg = 0
            iz_end = nabc['top']
            
            conn = np.zeros((n_elem,self.elem_nod), dtype = np.int32)
            conn[:,0] = nod_id[iz_beg:iz_end,ix_beg:ix_end].reshape(1, n_elem, order = 'F')
            conn[:,1] = nod_id[iz_beg:iz_end,ix_beg+1:ix_end+1].reshape(1,n_elem, order = 'F')
            conn[:,2] = nod_id[iz_beg+1:iz_end+1,ix_beg+1:ix_end+1].reshape(1,n_elem, order = 'F')
            conn[:,3] = nod_id[iz_beg+1:iz_end+1,ix_beg:ix_end].reshape(1,n_elem, order = 'F')
           
            x0 = xmin + dx/2.0
            z0 = zmin + dz/2.0
            rep_coord = np.ones((n_elem,1)) * np.array([x0,z0])
            self.elemset.append({'n_elem': n_elem, 'elem_type': elem_type, 'conn': conn, 'rep_coord': rep_coord, 'set_loc': 'top-left'})

        # top center
        if nabc['top']>0:
            ielemset += 1
            n_elem =  nx *  nabc['top']
            elem_type = 'top' 
      
            # nodal connectivity
            ix_beg = nabc['left']
            ix_end = nabc['left'] + nx
            iz_beg = 0
            iz_end = nabc['top']
            
            conn = np.zeros((n_elem,self.elem_nod), dtype = np.int32)
            conn[:,0] = nod_id[iz_beg:iz_end,ix_beg:ix_end].reshape(1, n_elem, order = 'F')
            conn[:,1] = nod_id[iz_beg:iz_end,ix_beg+1:ix_end+1].reshape(1,n_elem, order = 'F')
            conn[:,2] = nod_id[iz_beg+1:iz_end+1,ix_beg+1:ix_end+1].reshape(1,n_elem, order = 'F')
            conn[:,3] = nod_id[iz_beg+1:iz_end+1,ix_beg:ix_end].reshape(1,n_elem, order = 'F')
            
            x0 = np.linspace(xmin+dx/2.0, xmax-dx/2.0, num = nx).repeat(nabc['top'])
            z0 = (zmin+dz/2.0) * np.ones((1,n_elem))
            rep_coord = np.zeros((n_elem,2))
            rep_coord[:,0] = x0
            rep_coord[:,1] = z0            
            self.elemset.append({'n_elem': n_elem, 'elem_type': elem_type, 'conn': conn, 'rep_coord': rep_coord, 'set_loc': 'top'})
                  
        # right top corner
        if nabc['right']>0 and nabc['top']>0:
            ielemset += 1
            n_elem =  nabc['right'] *  nabc['top']
            elem_type = 'corner' 
      
            # nodal connectivity
            ix_beg = nabc['left'] + nx
            ix_end = nabc['left'] + nx + nabc['right']
            iz_beg = 0
            iz_end = nabc['top']
            
            conn = np.zeros((n_elem,self.elem_nod), dtype = np.int32)
            conn[:,0] = nod_id[iz_beg:iz_end,ix_beg:ix_end].reshape(1, n_elem, order = 'F')
            conn[:,1] = nod_id[iz_beg:iz_end,ix_beg+1:ix_end+1].reshape(1,n_elem, order = 'F')
            conn[:,2] = nod_id[iz_beg+1:iz_end+1,ix_beg+1:ix_end+1].reshape(1,n_elem, order = 'F')
            conn[:,3] = nod_id[iz_beg+1:iz_end+1,ix_beg:ix_end].reshape(1,n_elem, order = 'F')
            
            x0 = xmax-dx/2.0
            z0 = zmin+dz/2.0
            rep_coord = np.ones((n_elem,1)) * np.array([x0,z0])
                        
            self.elemset.append({'n_elem': n_elem, 'elem_type': elem_type, 'conn': conn, 'rep_coord': rep_coord, 'set_loc': 'top-right'})
                   
        # left side
        if nabc['left']>0:
            ielemset += 1
            n_elem =  nabc['left'] *  nz
            elem_type = 'side' 
      
            # nodal connectivity
            ix_beg = 0
            ix_end = nabc['left']
            iz_beg = nabc['top']
            iz_end = nabc['top'] + nz
            
            conn = np.zeros((n_elem,self.elem_nod), dtype = np.int32)
            conn[:,0] = nod_id[iz_beg:iz_end,ix_beg:ix_end].reshape(1, n_elem, order = 'F')
            conn[:,1] = nod_id[iz_beg:iz_end,ix_beg+1:ix_end+1].reshape(1,n_elem, order = 'F')
            conn[:,2] = nod_id[iz_beg+1:iz_end+1,ix_beg+1:ix_end+1].reshape(1,n_elem, order = 'F')
            conn[:,3] = nod_id[iz_beg+1:iz_end+1,ix_beg:ix_end].reshape(1,n_elem, order = 'F')
            
            x0 = (xmin+dx/2.0) * np.ones((1,n_elem))
            z0 = np.linspace(zmin+dz/2.0, zmax-dz/2.0, num = nz)[:,np.newaxis].repeat(nabc['left'], axis = 1).reshape((1,n_elem), order = 'F')
            rep_coord = np.zeros((n_elem,2))
            rep_coord[:,0] = x0
            rep_coord[:,1] = z0              
            
            self.elemset.append({'n_elem': n_elem, 'elem_type': elem_type, 'conn': conn, 'rep_coord': rep_coord, 'set_loc': 'left'})
        
        # center

        ielemset += 1
        n_elem =  nx * nz
        elem_type = 'center' 
      
        # nodal connectivity
        ix_beg = nabc['left']
        ix_end = nabc['left'] + nx
        iz_beg = nabc['top']
        iz_end = nabc['top'] + nz
            
        conn = np.zeros((n_elem,self.elem_nod), dtype = np.int32)
        conn[:,0] = nod_id[iz_beg:iz_end,ix_beg:ix_end].reshape(1, n_elem, order = 'F')
        conn[:,1] = nod_id[iz_beg:iz_end,ix_beg+1:ix_end+1].reshape(1,n_elem, order = 'F')
        conn[:,2] = nod_id[iz_beg+1:iz_end+1,ix_beg+1:ix_end+1].reshape(1,n_elem, order = 'F')
        conn[:,3] = nod_id[iz_beg+1:iz_end+1,ix_beg:ix_end].reshape(1,n_elem, order = 'F')
        
        x0 = np.linspace(xmin+dx/2.0, xmax-dx/2.0, num = nx)
        z0 = np.linspace(zmin+dz/2.0, zmax-dz/2.0, num = nz)
        x0, z0 = np.meshgrid(x0,z0)
        rep_coord = np.zeros((n_elem,2))
        rep_coord[:,0] = x0.reshape((1,n_elem), order = 'F')
        rep_coord[:,1] = z0.reshape((1,n_elem), order = 'F')        
        
        self.elemset.append({'n_elem': n_elem, 'elem_type': elem_type, 'conn': conn, 'rep_coord': rep_coord, 'set_loc': 'center'})
                    
        # right side
        if nabc['right']>0:
            ielemset += 1
            n_elem =  nabc['right'] *  nz
            elem_type = 'side' 
      
            # nodal connectivity
            ix_beg = nabc['left'] + nx
            ix_end = nabc['left'] + nx + nabc['right']
            iz_beg = nabc['top']
            iz_end = nabc['top'] + nz
            
            conn = np.zeros((n_elem,self.elem_nod), dtype = np.int32)
            conn[:,0] = nod_id[iz_beg:iz_end,ix_beg:ix_end].reshape(1, n_elem, order = 'F')
            conn[:,1] = nod_id[iz_beg:iz_end,ix_beg+1:ix_end+1].reshape(1,n_elem, order = 'F')
            conn[:,2] = nod_id[iz_beg+1:iz_end+1,ix_beg+1:ix_end+1].reshape(1,n_elem, order = 'F')
            conn[:,3] = nod_id[iz_beg+1:iz_end+1,ix_beg:ix_end].reshape(1,n_elem, order = 'F')
 
            x0 = (xmax-dx/2.0) * np.ones((1,n_elem))
            z0 = np.linspace(zmin+dz/2.0, zmax-dz/2.0, num = nz)[:,np.newaxis].repeat(nabc['right'], axis = 1).reshape((1,n_elem), order = 'F')
            rep_coord = np.zeros((n_elem,2))
            rep_coord[:,0] = x0
            rep_coord[:,1] = z0              
            
            self.elemset.append({'n_elem': n_elem, 'elem_type': elem_type, 'conn': conn, 'rep_coord': rep_coord, 'set_loc': 'right'})
                  
        # bottom left corner
        if nabc['left']>0 and nabc['bottom']>0:
            ielemset += 1
            n_elem =  nabc['left'] *  nabc['bottom']
            elem_type = 'corner' 
      
            # nodal connectivity
            ix_beg = 0
            ix_end = nabc['left']
            iz_beg = nabc['top'] + nz
            iz_end = nabc['top'] + nz + nabc['bottom']
            
            conn = np.zeros((n_elem,self.elem_nod), dtype = np.int32)
            conn[:,0] = nod_id[iz_beg:iz_end,ix_beg:ix_end].reshape(1, n_elem, order = 'F')
            conn[:,1] = nod_id[iz_beg:iz_end,ix_beg+1:ix_end+1].reshape(1,n_elem, order = 'F')
            conn[:,2] = nod_id[iz_beg+1:iz_end+1,ix_beg+1:ix_end+1].reshape(1,n_elem, order = 'F')
            conn[:,3] = nod_id[iz_beg+1:iz_end+1,ix_beg:ix_end].reshape(1,n_elem, order = 'F')
            
            x0 = xmin + dx/2.0
            z0 = zmax - dz/2.0
            rep_coord = np.ones((n_elem,1)) * np.array([x0,z0])
            self.elemset.append({'n_elem': n_elem, 'elem_type': elem_type, 'conn': conn, 'rep_coord': rep_coord, 'set_loc': 'bottom-left'})          

        # bottom center
        if nabc['bottom']>0:
            ielemset += 1
            n_elem =  nx *  nabc['bottom']
            elem_type = 'bottom' 
      
            # nodal connectivity
            ix_beg = nabc['left']
            ix_end = nabc['left'] + nx
            iz_beg = nabc['top'] + nz
            iz_end = nabc['top'] + nz + nabc['bottom']
            
            conn = np.zeros((n_elem,self.elem_nod), dtype = np.int32)
            conn[:,0] = nod_id[iz_beg:iz_end,ix_beg:ix_end].reshape(1, n_elem, order = 'F')
            conn[:,1] = nod_id[iz_beg:iz_end,ix_beg+1:ix_end+1].reshape(1,n_elem, order = 'F')
            conn[:,2] = nod_id[iz_beg+1:iz_end+1,ix_beg+1:ix_end+1].reshape(1,n_elem, order = 'F')
            conn[:,3] = nod_id[iz_beg+1:iz_end+1,ix_beg:ix_end].reshape(1,n_elem, order = 'F')
            
            x0 = np.linspace(xmin+dx/2.0, xmax-dx/2.0, num = nx).repeat(nabc['bottom'])
            z0 = (zmax-dz/2.0) * np.ones((1,n_elem))
            rep_coord = np.zeros((n_elem,2))
            rep_coord[:,0] = x0
            rep_coord[:,1] = z0            
            self.elemset.append({'n_elem': n_elem, 'elem_type': elem_type, 'conn': conn, 'rep_coord': rep_coord, 'set_loc': 'bottom'})
                  
        # bottom right corner
        if nabc['right']>0 and nabc['bottom']>0:
            ielemset += 1
            n_elem =  nabc['right'] *  nabc['bottom']
            elem_type = 'corner' 
      
            # nodal connectivity
            ix_beg = nabc['left'] + nx
            ix_end = nabc['left'] + nx + nabc['right']
            iz_beg = nabc['top'] + nz
            iz_end = nabc['top'] + nz + nabc['bottom']
            
            conn = np.zeros((n_elem,self.elem_nod), dtype = np.int32)
            conn[:,0] = nod_id[iz_beg:iz_end,ix_beg:ix_end].reshape(1, n_elem, order = 'F')
            conn[:,1] = nod_id[iz_beg:iz_end,ix_beg+1:ix_end+1].reshape(1,n_elem, order = 'F')
            conn[:,2] = nod_id[iz_beg+1:iz_end+1,ix_beg+1:ix_end+1].reshape(1,n_elem, order = 'F')
            conn[:,3] = nod_id[iz_beg+1:iz_end+1,ix_beg:ix_end].reshape(1,n_elem, order = 'F')
            
            x0 = xmax - dx/2.0
            z0 = zmax - dz/2.0
            rep_coord = np.ones((n_elem,1)) * np.array([x0,z0])
            self.elemset.append({'n_elem': n_elem, 'elem_type': elem_type, 'conn': conn, 'rep_coord': rep_coord, 'set_loc': 'bottom-right'})
                                              
        pass        
    
    def get_matrix(self, **analysis_data):
        # called in first frequency only to construct sparse matrix indices and initialized elements matrices
        
        import Imaging
        import numpy as np
        
        freq = analysis_data['freq']
        velocity_model = analysis_data['velocity_model']
        self.is_init = True
        
        num_elemset = len(self.elemset)
        ttl_num_elem = 0
        
        for iset in np.arange(num_elemset):
            ttl_num_elem += self.elemset[iset]['n_elem']
        
        # element matrices
        
        if self.is_init:
            self.elem_stiff = np.zeros((ttl_num_elem,16), dtype = np.complex128)
            self.elem_mass = np.zeros((ttl_num_elem,16), dtype = np.complex128)
            i_spr_ind = np.zeros((ttl_num_elem,16),  dtype = np.int32)
            j_spr_ind = np.zeros((ttl_num_elem,16),  dtype = np.int32)
                        
        ielem = 0
        # FixMe: this part will be modified if the density is not constant
        
        for iset in np.arange(num_elemset):
            
            mass, stiff1, stiff2 = Imaging.Element.get_matrix(self.elemset[iset]['elem_type'])
            
            vel = velocity_model.get_velocity(self.elemset[iset]['rep_coord'])[:,np.newaxis] # velocity
            if self.elemset[iset]['set_loc'] == 'top-left':
                
                a = self.mesh_data['abc']['left']['a']
                b = self.mesh_data['abc']['left']['b']
                 
                hx = 2.0j * ( a.real + 1.0j * a.imag * np.sign(-freq) )/(-freq) + b
                hx = hx.repeat(self.mesh_data['abc']['top']['nabc'], axis = 0)
                
                a = self.mesh_data['abc']['top']['a']
                b = self.mesh_data['abc']['top']['b']               

                hz = 2.0j * ( a.real + 1.0j * a.imag * np.sign(-freq) )/(-freq) + b
                hz = hz.repeat(self.mesh_data['abc']['left']['nabc'], axis = 1).reshape( (self.elemset[iset]['n_elem'],1), order = 'F')
                
            elif self.elemset[iset]['set_loc'] == 'top':
                
                hx = self.mesh_data['mesh']['hor']['dx'] * np.ones((self.elemset[iset]['n_elem'],1), dtype = np.float64)
                
                a = self.mesh_data['abc']['top']['a']
                b = self.mesh_data['abc']['top']['b']
                
                hz = 2.0j * ( a.real + 1.0j * a.imag * np.sign(-freq) )/(-freq) + b
                hz = hz.repeat(self.mesh_data['mesh']['hor']['nx'], axis = 1).reshape( (self.elemset[iset]['n_elem'],1), order = 'F')  
   
            elif self.elemset[iset]['set_loc'] == 'top-right':
                
                a = self.mesh_data['abc']['right']['a']
                b = self.mesh_data['abc']['right']['b']
                 
                hx = 2.0j * ( a.real + 1.0j * a.imag * np.sign(-freq) )/(-freq) + b
                hx = hx.repeat(self.mesh_data['abc']['top']['nabc'], axis = 0)
                
                a = self.mesh_data['abc']['top']['a']
                b = self.mesh_data['abc']['top']['b']               

                hz = 2.0j * ( a.real + 1.0j * a.imag * np.sign(-freq) )/(-freq) + b
                hz = hz.repeat(self.mesh_data['abc']['right']['nabc'], axis = 1).reshape( (self.elemset[iset]['n_elem'],1), order = 'F')                
                
            elif self.elemset[iset]['set_loc'] == 'left':
                
                a = self.mesh_data['abc']['left']['a']
                b = self.mesh_data['abc']['left']['b']
                 
                hx = 2.0j * ( a.real + 1.0j * a.imag * np.sign(-freq) )/(-freq) + b
                hx = hx.repeat(self.mesh_data['mesh']['ver']['nz'], axis = 0).reshape( (self.elemset[iset]['n_elem'],1), order = 'C')                                
                
                hz = self.mesh_data['mesh']['ver']['dz'] * np.ones((self.elemset[iset]['n_elem'],1), dtype = np.float64)
                
            elif self.elemset[iset]['set_loc'] == 'center' and self.is_init:
                
                hx = self.mesh_data['mesh']['hor']['dx'] * np.ones((self.elemset[iset]['n_elem'],1), dtype = np.float64)                
                hz = self.mesh_data['mesh']['ver']['dz'] * np.ones((self.elemset[iset]['n_elem'],1), dtype = np.float64)
                
            elif self.elemset[iset]['set_loc'] == 'right':
                
                a = self.mesh_data['abc']['right']['a']
                b = self.mesh_data['abc']['right']['b']
                 
                hx = 2.0j * ( a.real + 1.0j * a.imag * np.sign(-freq) )/(-freq) + b
                hx = hx.repeat(self.mesh_data['mesh']['ver']['nz'], axis = 0).reshape( (self.elemset[iset]['n_elem'],1), order = 'C')                                
                
                hz = self.mesh_data['mesh']['ver']['dz'] * np.ones((self.elemset[iset]['n_elem'],1), dtype = np.float64)                
                
            elif self.elemset[iset]['set_loc'] == 'bottom-left':
                
                a = self.mesh_data['abc']['left']['a']
                b = self.mesh_data['abc']['left']['b']
                 
                hx = 2.0j * ( a.real + 1.0j * a.imag * np.sign(-freq) )/(-freq) + b
                hx = hx.repeat(self.mesh_data['abc']['bottom']['nabc'], axis = 0)
                
                a = self.mesh_data['abc']['bottom']['a']
                b = self.mesh_data['abc']['bottom']['b']               

                hz = 2.0j * ( a.real + 1.0j * a.imag * np.sign(-freq) )/(-freq) + b
                hz = hz.repeat(self.mesh_data['abc']['left']['nabc'], axis = 1).reshape( (self.elemset[iset]['n_elem'],1), order = 'F')
                
            elif self.elemset[iset]['set_loc'] == 'bottom':
                
                hx = self.mesh_data['mesh']['hor']['dx'] * np.ones((self.elemset[iset]['n_elem'],1), dtype = np.float64)
                
                a = self.mesh_data['abc']['bottom']['a']
                b = self.mesh_data['abc']['bottom']['b']
                
                hz = 2.0j * ( a.real + 1.0j * a.imag * np.sign(-freq) )/(-freq) + b
                hz = hz.repeat(self.mesh_data['mesh']['hor']['nx'], axis = 1).reshape( (self.elemset[iset]['n_elem'],1), order = 'F')                  
                                
                
            elif self.elemset[iset]['set_loc'] == 'bottom-right':
                
                a = self.mesh_data['abc']['right']['a']
                b = self.mesh_data['abc']['right']['b']
                 
                hx = 2.0j * ( a.real + 1.0j * a.imag * np.sign(-freq) )/(-freq) + b
                hx = hx.repeat(self.mesh_data['abc']['bottom']['nabc'], axis = 0)
                
                a = self.mesh_data['abc']['bottom']['a']
                b = self.mesh_data['abc']['bottom']['b']               

                hz = 2.0j * ( a.real + 1.0j * a.imag * np.sign(-freq) )/(-freq) + b
                hz = hz.repeat(self.mesh_data['abc']['right']['nabc'], axis = 1).reshape( (self.elemset[iset]['n_elem'],1), order = 'F')                                
                
            else:
                if self.is_init:
                    raise Exception('undefined element set location')
                    
            if self.elemset[iset]['set_loc'] != 'center' or self.is_init:
                self.elem_stiff[ielem:ielem+self.elemset[iset]['n_elem'],:] = hz/hx * stiff1 + hx/hz * stiff2
                self.elem_mass[ielem:ielem+self.elemset[iset]['n_elem'],:] = hx*hz/vel**2 * mass 
            
            if self.is_init:
                DOF1 = self.elemset[iset]['conn'][:,0][:, np.newaxis]
                DOF2 = self.elemset[iset]['conn'][:,1][:, np.newaxis]
                DOF3 = self.elemset[iset]['conn'][:,2][:, np.newaxis]
                DOF4 = self.elemset[iset]['conn'][:,3][:, np.newaxis]
                
                i_spr_ind[ielem:ielem+self.elemset[iset]['n_elem'],:] = np.concatenate( (DOF1, DOF2, DOF3, DOF4), axis = 1).repeat(4, axis = 1) 
                j_spr_ind[ielem:ielem+self.elemset[iset]['n_elem'],:] = np.tile( np.concatenate( (DOF1, DOF2, DOF3, DOF4), axis = 1), (1,4))
                
            # next elemenet set    
            ielem += self.elemset[iset]['n_elem']
            
        if self.is_init:
            self.ij_spr_ind = np.zeros((2,ttl_num_elem * 16),  dtype = np.int32)
            self.ij_spr_ind[0,:] = i_spr_ind.ravel()
            self.ij_spr_ind[1,:] = j_spr_ind.ravel()            
            
        pass        
    