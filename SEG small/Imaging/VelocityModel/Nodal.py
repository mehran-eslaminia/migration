class NodalVelocityModel:
    def __init__(self, in_velocity):
        from scipy import interpolate as intp
        import numpy as np
        
        self.params = in_velocity['background']['value']
        self.num_params = in_velocity['background']['num_params']
        self.coord = in_velocity['background']['coord']
#        self.function = intp.Rbf(self.coord[:,0],self.coord[:,1],self.params, function = 'linear')
                
        pts = np.array((self.coord[:,0].ravel(), self.coord[:,1].ravel())).T
        value = self.params.ravel()
        
        self.function = intp.LinearNDInterpolator(pts,value)
        

    
    def get_velocity(self,out_coord):
        out_velocity = self.function(out_coord[:,0],out_coord[:,1])
        return out_velocity
        
        