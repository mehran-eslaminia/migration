# running 

import Imaging
import numpy as np
import matplotlib.pylab as plt
import pickle
import scipy.io

foldername = "./small_SEG/"
filename = 'SEG_section_340'

input_data = Imaging.Input.get(filename,foldername)

input_data['receiver']['interior'] = False
input_data['receiver']['surface'] = False
input_data['receiver']['point'] = True

full_wave_solver = Imaging.FullWaveDirectSolver(input_data)

# velocity model
model_type = input_data['velocity_model']['background']['type']

if model_type == 'nodal':
    velocity_model = Imaging.NodalVelocityModel(input_data['velocity_model'])
    
elif model_type == 'interp':
    pass
    
elif model_type == 'layer':
    pass
    
else:
    raise Exception("Undefined velocity model!") 

Imaging.Plotting.plot_velocity(velocity_model,input_data)

output = full_wave_solver.run_analysis('Forward',velocity_model) 

output_file = foldername + filename + '.mat'
scipy.io.savemat(output_file, {'point': output['point']})
"""

Imaging.Plotting.plot_velocity(velocity_model,input_data)
            
output0 = full_wave_solver.run_analysis('Forward',velocity_model)     
#output1 = one_way_solver.run_analysis('Forward',velocity_model)
output2 = true_amp_solver.run_analysis('Forward',velocity_model)

Imaging.Plotting.plot_int_disp(output0, input_data, freq_no = 1, source_no = 1) 
#Imaging.Plotting.plot_int_disp(output1, input_data, freq_no = 1, source_no = 2)  
Imaging.Plotting.plot_int_disp(output2, input_data, freq_no = 1, source_no = 1)

#Imaging.Plotting.plot_int_error(output1,output0, input_data, freq_no = 1, source_no = 1) 
Imaging.Plotting.plot_int_error(output2,output0, input_data, freq_no = 1, source_no = 1)  
"""

"""
time_ind = np.arange(input_data['mesh']['time']['nt'], dtype = np.int16)
value = full_wave_solver.source[0].get_value_time(time_ind)

hf = plt.figure()
hf.add_subplot(1,1,1)
plt.plot(input_data['mesh']['time']['t'], value)
hf.show()

value = full_wave_solver.source[0].get_value_freq(time_ind)
hf = plt.figure()
hf.add_subplot(1,2,1)
plt.plot(input_data['mesh']['freq']['w'], value.real)
hf.add_subplot(1,2,2)
plt.plot(input_data['mesh']['freq']['w'], value.imag)
hf.show()

# solving
output = full_wave_solver.run_analysis('Forward',velocity_model)
Imaging.Plotting.plot_int_disp(output, input_data, freq_no = 1, source_no = 1)
Imaging.Plotting.plot_surf_disp(output, input_data, freq_no = 1, source_no = 1)
#print full_wave_solver.mesh.active_nod
"""
"""
mass, stiff1, stiff2 = Imaging.Element.get_matrix('top')

print 'mass = ' , mass
print 'stiff1 = ' , stiff1
print 'stiff2 = ' , stiff2
"""