# running 

import Imaging
import numpy as np
import matplotlib.pylab as plt
# import pickle
import copy
import scipy.io 

# load sruface trace
foldername = "./small_SEG/"
filename = 'SEG_section_340'

input_file = foldername + filename + '.mat'
input_trace = scipy.io.loadmat(input_file)

input_data = Imaging.Input.get(filename,foldername)

#full_wave_migration = Imaging.FullWaveMigration(input_data, 'direct')
#one_way_migration = Imaging.OneWayMigration(input_data, 'standard')
true_amp_migration = Imaging.OneWayMigration(input_data, 'true-amplitude')
#two_sweep_true_amp_migration = Imaging.OneWayMigration(input_data, 'true-amplitude-2sweep')

# velocity model
model_type = input_data['velocity_model']['background']['type']

if model_type == 'nodal':
    velocity_model = Imaging.NodalVelocityModel(input_data['velocity_model'])
    
elif model_type == 'interp':
    pass
    
elif model_type == 'layer':
    pass
     
else:
    raise Exception("Undefined velocity model!") 
    
Imaging.Plotting.plot_velocity(velocity_model,input_data)

# full wave migration
"""
full_wave_migration.get_trace(velocity_model,copy.deepcopy(input_trace))
full_wave_migration.get_image(velocity_model)

migration_file = foldername + 'full_wave_migration_' + filename + '.mat'
scipy.io.savemat(migration_file, {'image':full_wave_migration.image})
"""

"""
# standard one way migration
one_way_migration.get_trace(velocity_model,copy.deepcopy(input_trace))
one_way_migration.get_image(velocity_model)

migration_file = foldername + 'standard_one_way_migration_' + filename + '.mat'
scipy.io.savemat(migration_file, {'image':one_way_migration.image})

"""
# true amplitude one way migration
true_amp_migration.get_trace(velocity_model,copy.deepcopy(input_trace))
true_amp_migration.get_image(velocity_model)

migration_file = foldername + 'true_amp_one_way_migration_' + filename + '.mat'
scipy.io.savemat(migration_file, {'image':true_amp_migration.image})

"""
# 2sweep true amplitude one way migration
two_sweep_true_amp_migration.get_trace(velocity_model,copy.deepcopy(input_trace))
two_sweep_true_amp_migration.get_image(velocity_model)

migration_file = foldername + '2sweep_true_amp_one_way_migration_' + filename + '.mat'
scipy.io.savemat(migration_file, {'image':two_sweep_true_amp_migration.image})

Imaging.Plotting.plot_image(one_way_migration.image, input_data, title = 'standard one way')

Imaging.Plotting.plot_image(two_sweep_true_amp_migration.image, input_data, title = 'true amplitude one way 2 sweep')

Imaging.Plotting.plot_image(full_wave_migration.image, input_data, title = 'full wave')
"""

Imaging.Plotting.plot_image(true_amp_migration.image, input_data, title = 'true amplitude one way')

"""

Imaging.Plotting.plot_velocity(velocity_model,input_data)
            
output0 = full_wave_solver.run_analysis('Forward',velocity_model)     
#output1 = one_way_solver.run_analysis('Forward',velocity_model)
output2 = true_amp_solver.run_analysis('Forward',velocity_model)

Imaging.Plotting.plot_int_disp(output0, input_data, freq_no = 1, source_no = 1) 
#Imaging.Plotting.plot_int_disp(output1, input_data, freq_no = 1, source_no = 2)  
Imaging.Plotting.plot_int_disp(output2, input_data, freq_no = 1, source_no = 1)

#Imaging.Plotting.plot_int_error(output1,output0, input_data, freq_no = 1, source_no = 1) 
Imaging.Plotting.plot_int_error(output2,output0, input_data, freq_no = 1, source_no = 1)  
"""

"""
time_ind = np.arange(input_data['mesh']['time']['nt'], dtype = np.int16)
value = full_wave_solver.source[0].get_value_time(time_ind)

hf = plt.figure()
hf.add_subplot(1,1,1)
plt.plot(input_data['mesh']['time']['t'], value)
hf.show()

value = full_wave_solver.source[0].get_value_freq(time_ind)
hf = plt.figure()
hf.add_subplot(1,2,1)
plt.plot(input_data['mesh']['freq']['w'], value.real)
hf.add_subplot(1,2,2)
plt.plot(input_data['mesh']['freq']['w'], value.imag)
hf.show()

# solving
output = full_wave_solver.run_analysis('Forward',velocity_model)
Imaging.Plotting.plot_int_disp(output, input_data, freq_no = 1, source_no = 1)
Imaging.Plotting.plot_surf_disp(output, input_data, freq_no = 1, source_no = 1)
#print full_wave_solver.mesh.active_nod
"""
"""
mass, stiff1, stiff2 = Imaging.Element.get_matrix('top')

print 'mass = ' , mass
print 'stiff1 = ' , stiff1
print 'stiff2 = ' , stiff2
"""