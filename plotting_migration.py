# load and plot migration
import Imaging
import numpy as np
import matplotlib.pylab as plt
import scipy.io

foldername = "./Examples/"
filename = 'example002_1'

input_data = Imaging.Input.get(filename,foldername)

# velocity model
model_type = input_data['velocity_model']['background']['type']

if model_type == 'nodal':
    velocity_model = Imaging.NodalVelocityModel(input_data['velocity_model'])
    
elif model_type == 'interp':
    pass
    
elif model_type == 'layer':
    pass
    
else:
    raise Exception("Undefined velocity model!") 

Imaging.Plotting.plot_velocity(velocity_model,input_data)

# full wave
migration_file = foldername + 'full_wave_migration_' + filename + '.mat'
data = scipy.io.loadmat(migration_file)
Imaging.Plotting.plot_image(data['image'], input_data, title = 'full wave')

# true amplitude one way
migration_file = foldername + 'true_amp_one_way_migration_' + filename + '.mat'
data = scipy.io.loadmat(migration_file)
Imaging.Plotting.plot_image(data['image'], input_data, title = 'true amplitude one way')

# standard one way migration
migration_file = foldername + 'standard_one_way_migration_' + filename + '.mat'
data = scipy.io.loadmat(migration_file)
Imaging.Plotting.plot_image(data['image'], input_data, title = 'standard one way')

# standard one way migration
migration_file = foldername + '2sweep_true_amp_one_way_migration_' + filename + '.mat'
data = scipy.io.loadmat(migration_file)
Imaging.Plotting.plot_image(data['image'], input_data, title = 'true amplitude one way 2 sweep')
